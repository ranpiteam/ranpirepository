import React, { Component, PropTypes } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Linking, setState, ScrollView } from 'react-native';
import SignIn from '../../components/SignIn/SignIn';
import { styles } from './styles';

export default class Registration extends Component {

  static navigationOptions = {
    header: null
  }

  render() {
    return (
      <ScrollView
        keyboardShouldPersistTaps='handled'
        scrollEnabled={false}
        contentContainerStyle={styles.container}>

        <View style={styles.emptyContainer}></View>
        <View style={styles.logoArea}>
          <Text style={styles.logoText}>RanPi</Text>
        </View>

        <View style={styles.form}>
          <SignIn navigation={this.props.navigation} ></SignIn>
        </View>

        <View style={styles.lang}>
          <TouchableOpacity style={styles.link} disabled={true} >
            <Text style={styles.activeText}>
              English
            </Text>
          </TouchableOpacity>
          <Text style={styles.text}>{' - '}</Text>
          <TouchableOpacity style={styles.link} disabled={true} >
            <Text style={styles.text}>
              العربية
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

