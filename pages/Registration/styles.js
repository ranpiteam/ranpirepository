import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: '#7b68ee',
    justifyContent: 'space-between',
  },
  emptyContainer: {
    flex: 1,
  },
  logoArea: {
    flex: 1,
    backgroundColor: '#9D8EF7',
    justifyContent: 'center',
    borderWidth: 2,
    borderRadius: 7,
    borderColor: '#32229B',
    elevation: 3
  },
  logoText: {
    fontFamily: 'serif',
    color: 'white',
    fontSize: 50,
    textAlign: 'center',
    fontWeight: 'bold',
    fontStyle: 'italic'
  },
  form: {
    flex: 3
  },
  button: {
    margin: 10,
    color: '#ec1369',
  },
  link: {
    alignItems: 'center',
  },
  lang: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingBottom: 5
  },
  text: {
    color: '#b0c4de',
  },
  activeText: {
    color: 'white',
  }
});

export { styles }