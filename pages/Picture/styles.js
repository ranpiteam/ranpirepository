import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7b68ee',
    justifyContent: 'space-between'
  },
  image: {
    width: '90%',
    height: '100%',
    backgroundColor: '#9D8EF7',
    borderWidth: 3,
    borderRadius: 5,
    borderColor: '#32229B',
    alignSelf: 'center'
  },
  button: {
    flex: 0.5,
    marginTop: 2,
    marginBottom: 2,
    width: '90%',
    borderRadius: 5,
    alignSelf: 'center',
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffc0cb',
    elevation: 7
  },
  indicator: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
  },
  input: {
    flex: 2,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: '90%'
  }
});

export { styles }