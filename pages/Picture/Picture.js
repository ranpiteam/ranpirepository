import React, { Component, PropTypes } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Linking, setState, ScrollView, Image, Button, Keyboard, KeyboardAvoidingView } from 'react-native';
import { styles } from './styles';
import CustomTextInput from '../../components/CustomTextInput';
import { ActivityIndicator } from 'react-native';

export default class Picture extends Component {

  interval = 0;

  static navigationOptions = {
    header: null
  }

  state = {
    imageUrl: '',
    time: 3000,
    loading: true
  }

  constructor(props) {
    super(props);
    this._isMounted = false;
    this.getImage();
  }

  componentDidMount() {
    this._isMounted = true;
    this.createInterval(this.state.time);
  }

  componentWillUnmount() {
    this._isMounted = false;
    clearInterval(this.interval);
  }

  createInterval(time) {
    this.interval = setInterval(() => { this.getImage() }, time)
  }

  validInputData(inputData) {
    inputData = Number(inputData);
    if (typeof inputData !== 'number' || isNaN(inputData)) {
      this.setState({ Error: 'It is not a number' });
      return false;
    } else if (inputData < 1 || inputData > 20) {
      this.setState({ Error: 'Enter a number between 1 and 20' });
      return false;
    } else if (!inputData) {
      this.setState({ Error: 'Enter a number' });
      return false;
    }
    this.setState({ Error: '' });
    return true;
  }

  onSubmit(num) {
    Keyboard.dismiss();
    if (this._isMounted && this.validInputData(num)) {
      clearInterval(this.interval);
      this.createInterval(num * 1000);
    }
  }

  async getImage() {
    fetch("https://dog.ceo/api/breeds/image/random").
      then(res => res.json()).
      then(data => { if (this._isMounted) this.setState({ imageUrl: data.message }) }).catch(err => console.log(err));
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps='handled' scrollEnabled={false} contentContainerStyle={styles.container} >

        <View style={{ flex: 0.5 }}></View>

        <View style={{ flex: 6 }}>
          <Image
            onLoadEnd={() => { this.setState({ loading: false }) }}
            onLoadStart={() => { this.setState({ loading: true }) }}
            style={styles.image}
            source={{ uri: this.state.imageUrl ? this.state.imageUrl : null }}
            resizeMode="cover">
          </Image>
          <ActivityIndicator style={styles.indicator} size="large" color="#0000ff" animating={this.state.loading} />
        </View>

        <KeyboardAvoidingView behavior="padding" style={styles.input} enabled >
          <Text style={styles.text}>Enter time in sec and submit</Text>
          <CustomTextInput keyboardType={'numeric'} onSubmitEvent={this.onSubmit.bind(this)} maxLength={3} />
          <Text style={{ color: "red", fontStyle: 'italic' }}>{this.state.Error}</Text>
        </KeyboardAvoidingView>

        <TouchableOpacity style={styles.button}
          onPress={() => { this.props.navigation.navigate("Login"); }}>
          <Text style={styles.text}>EXIT</Text>
        </TouchableOpacity>

        <View style={{ flex: 0.5 }}></View>

      </ScrollView >
    );
  }
}
