import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Registration from './pages/Registration/Registration';
import Picture from './pages/Picture/Picture';

const MainNavigator = createStackNavigator({
  Login: {
    screen: Registration
  },
  Main: {
    screen: Picture
  },
},
  {
    initialRouteName: 'Login'
  }
);

const AppContainer = createAppContainer(MainNavigator);

export default class App extends Component {
  render() {
    return <AppContainer />
  }
}
