import React, { Component, PropTypes } from 'react';
import { Text, View, setState, Button, TouchableOpacity, Linking, KeyboardAvoidingView } from 'react-native';
import CustomTextInput from '../CustomTextInput';
import { Icon } from 'react-native-elements';
import { styles, buttons } from './styles';

export default class SignIn extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      password: '',
    };
  }

  setName = (inputData) => {
    this.setState({ name: inputData })
  }

  setPassword = (inputData) => {
    this.setState({ password: inputData })
  }

  onSubmit = () => {
    this.setState({ ErrorName: (this.state.name === "") ? 'Please, fill the name field' : '' });
    this.setState({ ErrorPassword: (this.state.password === "") ? 'Please, fill the password field' : '' });

    if (this.state.password !== "" && this.state.name !== "") {
      this.setName('');
      this.setPassword('');
      this.props.navigation.navigate("Main");
    }
  }

  render() {
    return (
      <View style={styles.form}>

        <View style={{ flex: 1 }}></View>

        <KeyboardAvoidingView behavior="padding" style={{ flex: 2 }} enabled >
          <CustomTextInput placeholder="User name" setValue={this.setName} value={this.state.name} maxLength={20} />
          <Text style={styles.errorText}> {this.state.ErrorName} </Text>
          <CustomTextInput placeholder="Password" setValue={this.setPassword} value={this.state.password} maxLength={20} />
          <Text style={styles.errorText}> {this.state.ErrorPassword} </Text>
        </KeyboardAvoidingView>

        <TouchableOpacity style={styles.link} disabled={true} >
          <Text style={styles.text}>
            Forgot the password?
          </Text>
        </TouchableOpacity>

        <View style={{ flex: 2 }}>
          <TouchableOpacity onPress={this.onSubmit} style={[buttons.button, buttons.enabledButton, { marginBottom: 20 }]}>
            <Text style={styles.activeText}>SIGN IN</Text>
          </TouchableOpacity>

          <TouchableOpacity disabled={true} style={[buttons.button, buttons.disabledButton]}>
            <Text style={styles.text}>SIGN UP</Text>
          </TouchableOpacity>
        </View>

        <View style={{ flex: 0.5 }}></View>

        <TouchableOpacity /*onPress={() => this.props.navigation.navigate("Main")}*/ style={styles.link} disabled={true} >
          <Text style={styles.activeText}>
            SKIP REGISTRATION
          </Text>
          <Icon name="chevron-right" color="white" size={20}></Icon >
        </TouchableOpacity>


      </View>
    )
  }
}