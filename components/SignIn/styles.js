import { StyleSheet } from 'react-native'

const buttons = StyleSheet.create({
  button: {
    display: 'flex',
    height: 35,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  enabledButton: {
    backgroundColor: '#ffc0cb',
    elevation: 7,
  },
  disabledButton: {
    backgroundColor: '#7b68ee',
    borderWidth: 1,
    borderColor: '#b0c4de',
  }
});

const styles = StyleSheet.create({
  form: {
    flex: 1,
  },
  link: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
    flex: 1
  },
  text: {
    color: '#b0c4de',
  },
  activeText: {
    color: 'white',
  },
  errorText: {
    color: 'red',
    fontStyle: 'italic'
  }
});

export { styles, buttons }
