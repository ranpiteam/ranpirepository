import React, { Component, PropTypes } from 'react';
import { StyleSheet, setState, TextInput, Keyboard } from 'react-native';

export default class CustomTextInput extends Component {
  state = {
    isFocused: false
  }
  onFocusChange = () => {
    this.setState({ isFocused: true });
  }
  onBlurChange = () => {
    this.setState({ isFocused: false });
  }
  handleChange(text) {
    this.props.setValue(text);
  }

  render() {
    return (
      <TextInput
        value={this.props.value}
        onChangeText={this.props.setValue ? this.handleChange.bind(this) : null}
        onFocus={this.onFocusChange}
        onBlur={this.onBlurChange}
        onSubmitEditing={(event) => this.props.onSubmitEvent ? this.props.onSubmitEvent(event.nativeEvent.text) : Keyboard.dismiss()}
        underlineColorAndroid='transparent'
        placeholder={this.props.placeholder}
        style={[(this.state.isFocused) ? { borderBottomColor: '#2BC0C0' } : { borderBottomColor: '#DEF1F1' }, styles.textInput]}
        maxLength={this.props.maxLength}
        blurOnSubmit={false}
        autoFocus={false}
        autoCorrect={false}
        keyboardType={this.props.keyboardType}
      />

    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    borderBottomWidth: 1,
    height: 30,
    fontSize: 15,
    paddingLeft: 20,
    paddingRight: 20
  }
}
)







