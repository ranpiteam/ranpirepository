import {AppRegistry} from 'react-native'
import App from './App';
import { FormInput } from './components/FormInput';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
